const getSum = (str1, str2) => {
  if ([str1, str2].every((str) => typeof str === 'string' &&
    /^$|^\d+$/.test(str))) {
    return (Number(str1) + Number(str2)).toString();
  } else {
    return false;
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const result = listOfPosts.reduce((begin, next) => {
    if (next.author == authorName) {
      begin[0] += 1;
    }
    if (next.comments) {
      begin[1] += next.comments.reduce((b, n) => {
        if (n.author == authorName) {
          b++;
        }
        return b;
      }, 0);
    }
    return begin;
  }, [0, 0]);
  return `Post:${result[0]},comments:${result[1]}`;
};

const tickets = (people) => {
  return typeof people.reduce((begin, next) => {
    return (next != 25 && next - 25 > begin) ? '' : begin + 25;
  }, 0) === 'string' ? 'NO' : 'YES';
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
